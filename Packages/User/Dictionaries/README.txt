Always up to date Portuguese dictionaries can be obtained here:
http://natura.di.uminho.pt/wiki/doku.php?id=dicionarios:main

English dictionaries taken from:
https://github.com/marcoagpinto/aoo-mozilla-en-dict

The respective license for the dictionaries are available on the txt file
with the same name.

Related links:
https://github.com/titoBouzout/Dictionaries
https://github.com/kevina/wordlist