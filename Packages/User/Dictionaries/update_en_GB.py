#!/usr/bin/python
# -*- coding: utf-8 -*-

from utils import downloadFiles

urlReadme = ("https://raw.githubusercontent.com/marcoagpinto/"
             "aoo-mozilla-en-dict/master/en_GB%20(Marco%20Pinto)"
             "/README_en_GB.txt")

urlAff = ("https://raw.githubusercontent.com/marcoagpinto/"
          "aoo-mozilla-en-dict/master/en_GB%20(Marco%20Pinto)"
          "/en-GB.aff")

urlDic = ("https://raw.githubusercontent.com/marcoagpinto/"
          "aoo-mozilla-en-dict/master/en_GB%20(Marco%20Pinto)"
          "/en-GB.dic")

targets = {
  'en_GB.txt': urlReadme,
  'en_GB.aff': urlAff,
  'en_GB.dic': urlDic
}

downloadFiles(targets)

print('en_GB updated.')
