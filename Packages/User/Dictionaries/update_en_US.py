#!/usr/bin/python
# -*- coding: utf-8 -*-

from utils import downloadFiles

urlReadme = ("https://raw.githubusercontent.com/marcoagpinto/"
             "aoo-mozilla-en-dict/master/en_US%20(Kevin%20Atkinson)"
             "/README_en_US.txt")

urlAff = ("https://raw.githubusercontent.com/marcoagpinto/"
          "aoo-mozilla-en-dict/master/en_US%20(Kevin%20Atkinson)"
          "/en_US.aff")

urlDic = ("https://raw.githubusercontent.com/marcoagpinto/"
          "aoo-mozilla-en-dict/master/en_US%20(Kevin%20Atkinson)"
          "/en_US.dic")

targets = {
  'en_US.txt': urlReadme,
  'en_US.aff': urlAff,
  'en_US.dic': urlDic
}

downloadFiles(targets)

print('en_US updated.')
