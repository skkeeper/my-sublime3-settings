#!/usr/bin/python
# -*- coding: utf-8 -*-

from utils import downloadFile
from os import path
from os import walk
import tarfile
import tempfile
from shutil import copy
from shutil import rmtree


url = ("http://natura.di.uminho.pt/download/sources/Dictionaries/hunspell/"
       "LATEST/hunspell-pt_PT-latest.tar.gz")

# Create temporary directory.
tmpDir = tempfile.mkdtemp()

localName = path.join(tmpDir, 'hunspell.tar.gz')

# Download tar.gz
downloadFile(url, localName)

# Extract
tar = tarfile.open(localName, 'r:gz')
tar.extractall(tmpDir)
tar.close()

extractedDirectory = ''
for dirname, dirnames, filenames in walk(tmpDir):
    print('pt_PT dictionary version: ' + dirnames[0])
    extractedDirectory = path.join(tmpDir, dirnames[0])
    break

readmeFile = path.join(extractedDirectory, 'README_pt_PT.txt')
dicFile = path.join(extractedDirectory, 'pt_PT.dic')
affFile = path.join(extractedDirectory, 'pt_PT.aff')

# Copy dictionaries.
copy(readmeFile, 'pt_PT.txt')
copy(dicFile, 'pt_PT.dic')
copy(affFile, 'pt_PT.aff')

# Clean up.
rmtree(tmpDir)

print('pt_PT updated.')
