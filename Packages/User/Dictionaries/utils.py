from urllib.request import urlopen
from os import path
import tempfile
from shutil import copy
from shutil import rmtree


def downloadFile(remoteFile, targetName):
    g = urlopen(remoteFile)
    with open(targetName, 'b+w') as f:
        f.write(g.read())


def downloadFiles(targets):
    # Create temporary directory.
    tmpDir = tempfile.mkdtemp()

    for key, value in targets.items():
        localFile = path.join(tmpDir, key)
        downloadFile(value, localFile)
        copy(localFile, key)

    rmtree(tmpDir)
