# my Sublime Text 3 settings

My personal settings for the Sublime Text 3 editor. These are mainly used for
Javascript development, but also include basic support for other languages
like Python/Ruby/Dart/Markdown/Typescript/C++/etc.

To keep up with my work this is an evolving package. Support for new languages
might be added from time to time as I need them.

Pull requests will be accepted in case some sort of setting/package conflict
is found, but otherwise this repository fully reflects my personal choices.

## Installation

Clear your ``%appdata%/Sublime Text 3`` folder of all the files (you might
want to keep your license file in the Local folder).

Create a temporary folder somewhere and run the following in the console
(works on Windows/Linux):

```
git clone git@bitbucket.org:skkeeper/my-sublime3-settings.git
```

Next copy all the contents you downloaded into ``%appdata%/Sublime Text 3``.
Accept file overwriting if asked.

Run Sublime Text 3, don't worry about errors related to files not being present.

Install [Package Control](https://packagecontrol.io/installation) for
Sublime Text 3.

Restart Sublime Text 3.

Wait for all the messages about installing packages to stop.

## Requirements

* Fonts
  - [Fira Code](https://github.com/tonsky/FiraCode)
  - (alternative) [Mononoki](https://madmalik.github.io/mononoki/)
  - (alternative) [Office Code Pro](https://github.com/nathco/Office-Code-Pro/)
  - (alternative) [DejaVu Sans Mono](http://dejavu-fonts.org/)
* [Ctags](http://ctags.sourceforge.net/) on your PATH.
* **For Javascript support:** ``node`` in the PATH (plus ``jshint``,
 ``jscs``, ``standard`` and ``semistandard``).

```bash
npm install -g jshint
npm install -g jscs
npm install -g standard
npm install -g semistandard
```

* **For HTML support:** ``node`` in the PATH plus ``htmlhint``.

```bash
npm install -g htmlhint
```

* **To improve your writing:**
  * install the Node package ``textlint``;
  * install the PIP program called ``proselint``. This is not just for prose, but
  also documentation and technical writing.

```bash
npm -g install textlint textlint-rule-no-todo textlint-rule-rousseau textlint-rule-write-good
pip install proselint
```

* **For Markdown support:** Make sure ``ruby`` is in your PATH and install
 ``mdl``.

```bash
gem install mdl
```

* **For Dart support:** by default the configuration expects a Windows
 environment and the Dart SDK to be at `C:\\Tools\\dart\\dart-sdk`. If
 you're working on another system, changing that setting should suffice.

* **For SCSS support:** Make sure ``ruby`` is in your PATH and install
 ``scss-lint``.

```bash
gem install scss_lint
```

* **For Ruby support:** Make sure ``ruby`` is in your PATH and install
 ``ruby-lint`` and ``rubocop``.

```bash
gem install ruby-lint rubocop
```

* **For C++ support:**:

  * Install [CPPCheck](http://cppcheck.sourceforge.net/) and add it to the PATH.
  * Install Google's C++ linter (requires Python PIP):

```bash
pip install cpplint
```

## Notes

* Different linters for Javascript are installed by default but only
  ``semistandard`` is enabled. To use any of the others in a specific project
  override the SublimeLinter settings by creating a ``"SublimeLinter"`` node
  in the package settings file.
* The use of different color schemes per syntax is intentional. Some color
  schemes don't support certain languages adequately.
* This preferences might not work on low spec machines.
* This was not tested on Mac OSX.

## Inspiration

* [Package Control Syncing](https://packagecontrol.io/docs/syncing)
* [My Development Setup with Sublime Text](http://www.justgoscha.com/programming/2014/09/16/My-development-setup-with-Sublime.html)
