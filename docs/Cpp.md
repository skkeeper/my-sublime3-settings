# C++

## Tools

- [CPPCheck](http://cppcheck.sourceforge.net/)
- Google's C++ linter (requires Python PIP):

```bash
pip install cpplint
```
