# Javascript

## When creating a new project

- Choose between ```standard``` or ```semistandard``` codestyles.
- Import [this .jscsrc](https://gist.github.com/skkeeper/e1f3a88e4b9d9b1973e8)
 file to complement whatever codestyle you choose previously.
- Create ```.editorconfig``` file.
- Consider including a [Code of Conduct](http://contributor-covenant.org/).
- Consider licensing under [ISC](https://opensource.org/licenses/ISC).

## Debugging Node

While node-debug is a good solution,
[iron-node](https://github.com/s-a/iron-node) feels better to use.

```bash
npm install -g iron-node
iron-node index.js
```

Use the keyword ```debugger``` to create breakpoints manually.

Hit F5 to refresh.

